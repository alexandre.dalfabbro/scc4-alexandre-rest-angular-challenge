// Criacao do modulo principal da aplicacao
var appCliente = angular.module("appCliente",[]);

// Criacao de controllers
appCliente.controller("indexController", function($scope, $http){
	
	$scope.clientes = [];
	$scope.cliente = {};
	
	carregarClientes = function() {
		$http({method:'GET', url:'http://localhost:8080/clientes'})
		.then(function(response){
			$scope.clientes = response.data;
		}, function(response){
			console.log(response.data);
			console.log(response.status);		
		});
	};
	
	$scope.salvarCliente = function() {
		$http({method:'POST', url:'http://localhost:8080/clientes', data:$scope.cliente})
		.then(function(response){
			console.log(response.data);
			console.log(response.status);
			//$scope.clientes.push(response.data);
			carregarClientes();
			$scope.cancelarAlteracaoCliente();
		}, function(response){
			console.log(response.data);
			console.log(response.status);		
		});		
	}
	
	$scope.excluirCliente = function(cliente) {
		$http({method:'DELETE', url:'http://localhost:8080/clientes/' + cliente.id})
		.then(function(response){
			for(i=0; i<$scope.clientes.length; i++) {
				if($scope.clientes[i].id==cliente.id) {
					$scope.clientes.splice(i, 1);
					break;
				}
			}
		}, function(response){
			console.log(response.data);
			console.log(response.status);		
		});		
	}	

	$scope.alterarCliente = function(cli) {
		$scope.cliente = angular.copy(cli);		
	}	
	
	$scope.cancelarAlteracaoCliente = function() {
		$scope.cliente = {};
	}
	
	carregarClientes();
});