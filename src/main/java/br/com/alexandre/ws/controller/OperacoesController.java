package br.com.alexandre.ws.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.alexandre.ws.model.Cliente;

@RestController
public class OperacoesController {
	// End Points
	@RequestMapping("/listaReversa")
	public List<Integer> Op1(@RequestParam(value="lista") String lista ) {
		List<String> items = Arrays.asList(lista.replace("{","").replace("}","").split(","));
		Collections.reverse(items);
		List<Integer> reverseList = new ArrayList<>();
		for (int i=0; i<items.size();i++) {
			reverseList.add(Integer.parseInt(items.get(i)));
		}
		
		return reverseList;
	}
	
	@RequestMapping("/imprimirImpares")
	public String imprimirImpares(@RequestParam(value="lista") String lista ) {
		int c=0;
		String[] stringArray = lista.replace("{","").replace("}","").split(",");
		int[] intArray = new int[stringArray.length];
		List<Integer> oddList = new ArrayList<>();
		for (int i = 0; i < stringArray.length; i++) {
			String numberAsString = stringArray[i];
			intArray[i] = Integer.parseInt(numberAsString);
		}
		for (int i=0; i<intArray.length; i++) {
			if (intArray[i]%2 != 0) {
				oddList.add(intArray[i]);
			}
		}		
		return oddList.toString();
	}
	
	@RequestMapping("/imprimirPares")
	public String imprimirPares(@RequestParam(value="lista") String lista) {
		String[] stringArray = lista.replace("{", "").replace("}", "").split(",");
		int[] intArray = new int[stringArray.length];
		List<Integer> evenList = new ArrayList<>();
		for (int i=0; i<stringArray.length; i++) {
			String numberAsString = stringArray[i];
			intArray[i] = Integer.parseInt(numberAsString);			
		}
		for (int i=0; i<intArray.length; i++) {
			if (intArray[i]%2 == 0) {
				evenList.add(intArray[i]);
			}
		}
		return evenList.toString();
	}
	
	@RequestMapping("/tamanho")
	public String tamanhoPalavra(@RequestParam(value="palavra") String palavra) {
		int tam;
		tam = palavra.length();
		return String.valueOf(tam);
	}	
	
	@RequestMapping(value="/maiusculas")
	public ResponseEntity<String> converterMaiusculas(@RequestParam(value="palavra") String palavra) {
		return new ResponseEntity<>(palavra.toUpperCase(), HttpStatus.OK);
	}	
	
	@RequestMapping("/vogais")
	public String findVowels(@RequestParam(value="palavra") String palavra) {
		String vogais = "";
		for(int i=0; i<palavra.length(); i++) {
			if (palavra.charAt(i) == 'a' || palavra.charAt(i) == 'e' || palavra.charAt(i) == 'i' || palavra.charAt(i) == 'o' || palavra.charAt(i) == 'u') {
				vogais = vogais + palavra.substring(i,i+1);
			}
		}
		return vogais;
	}
	
	@RequestMapping("/consoantes")
	public String findNotVowels(@RequestParam(value="palavra") String palavra) {
		String consoantes = "";
		for(int i=0; i<palavra.length(); i++) {
			if (palavra.charAt(i) != 'a' && palavra.charAt(i) != 'e' && palavra.charAt(i) != 'i' && palavra.charAt(i) != 'o' && palavra.charAt(i) != 'u') {
				consoantes = consoantes + palavra.substring(i,i+1);
			}
		}
		return consoantes;
	}	

	@RequestMapping("/nomeBibliografico")
	public String nomeBibliografico(@RequestParam(value="nome") String nome) {
		String lastName = ""; 
		String firstName = "";
		String[] stringArray = nome.split(" ");
		lastName = stringArray[stringArray.length-1].toUpperCase();
		for(int i=0; i<stringArray.length-1;i++) {
			firstName = firstName + stringArray[i].substring(0, 1).toUpperCase() + stringArray[i].substring(1) + " ";
		}
		return lastName + ", " + firstName;		
	}
	
	@RequestMapping(value="/tioDirceu")
	public ResponseEntity<String> numNotas(@RequestParam(value="valor") String valor) {
		String resposta;
		
		Integer cash;
		
		Integer notas5 = 0;
		Integer notas3 = 0;
		
		Integer resto5 = 0;
		Integer quo5 = 0;
		
		Integer resto3 = 0;
		Integer quo3 = 0;
		
		cash = Integer.parseInt(valor);
		
		while (true) {
			resto5 = cash % 5;
			quo5 = cash / 5;
			if (quo5 > 0) {
				notas5 = quo5;
				while (true) {
					resto3 = resto5 % 3;
					quo3 = resto5 / 3;
					if (resto3 == 0) {
						notas3 = quo3;
						break;
					} else {
						resto5 = resto5 + 5;
						notas5 = notas5 - 1;
					}
				}	
			}
			break;
		}
		
		resposta = "Saque R$" + cash + ": " + notas3 + " nota(s) de R$3 e " + notas5 + " nota(s) de R$5";
		
		return new ResponseEntity<>(resposta, HttpStatus.OK);
	}

	@RequestMapping(value="/calculadora")
	public ResponseEntity<String> calcular(@RequestParam(value="num1") String num1, @RequestParam(value="den1") String den1, @RequestParam(value="num2") String num2, @RequestParam(value="den2") String den2, @RequestParam(value="op") String op) {
		String resultado = "";
		String opSignal = "";
		
		Integer num = 0;
		Integer den = 0;
		
		Integer inum1 = 0;
		Integer iden1 = 0;
		Integer inum2 = 0;
		Integer iden2 = 0;
		
		inum1 = Integer.parseInt(num1);
		iden1 = Integer.parseInt(den1);
		inum2 = Integer.parseInt(num2);
		iden2 = Integer.parseInt(den2);
		
		den = iden1*iden2;
		
		switch (op) {
			case "soma": {
				num = ((den)/iden1)*inum1 + ((den)/iden2)*inum2;				
				resultado = num + " / " + den;
				opSignal = " + ";
				break;
			}
			case "subtracao": {
				num = ((den)/iden1)*inum1 - ((den)/iden2)*inum2;				
				resultado = num + " / " + den;
				opSignal = " - ";
				break;
			}
			case "multiplicacao": {
				num = inum1 * inum2;
				opSignal = " * ";
				break;
			}
			case "divisao": {
				num = inum1 * iden2;
				den = iden1 * inum2;
				opSignal = " / ";
				break;
			}
		}
		
		resultado = num + " / " + den;
		
		return new ResponseEntity<>(num1 + "/" + den1 + opSignal + num2 + "/" + den2 + " = " + resultado, HttpStatus.OK);
	}	
	
}
